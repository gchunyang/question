from server import app_globals
from server.app_globals import db


class AppConfigBase(object):
    security_key = 'A0Zx98G/3y112323X&R~8H$!jmN#LWX/,?R;'
    sqlalchemy_echo = False
    sqlalchemy_pool_size = 20
    db_user_name = "root"
    db_password = "a"
    db_schema_name = "wenda"
    testing = False
    adm_users = ["wbingwei@gmail.com"]
    cache = None

    def get_mysql_url(self):
        return 'mysql://%s:%s@localhost/%s?charset=utf8' % (self.db_user_name, self.db_password, self.db_schema_name)

    def init_app(self, app):
        app.secret_key = self.security_key
        app.config['SQLALCHEMY_POOL_SIZE'] = self.sqlalchemy_pool_size
        app.config['SQLALCHEMY_DATABASE_URI'] = self.get_mysql_url()
        app.config['SQLALCHEMY_ECHO'] = self.sqlalchemy_echo
        app.config['TESTING'] = self.testing
        if self.cache is not None:
            app_globals.cache = self.cache
        db.init_app(app)