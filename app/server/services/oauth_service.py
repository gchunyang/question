# -*- coding: utf-8 -*-
from datetime import timedelta, datetime
import json

from flask import session, g

from server.services.database.user_tables import OAuthUserTable
from server.services.user_service import UserManager


OAUTH_QQ_PROVIDER = "Q"
OAUTH_GOOGLE_PROVIDER = "G"

QQ_OAUTH_SESSION_TOKEN_KEY = 'qq_oauth_token'
GOOGLE_OAUTH_SESSION_TOKEN_KEY = 'google_oauth_token'


def QQ(oauth, consumer_key, consumer_secret):
    return oauth.remote_app('qq',
                            base_url='https://graph.qq.com',
                            request_token_url=None,
                            authorize_url='oauth2.0/authorize',
                            access_token_url='oauth2.0/token',
                            consumer_key=consumer_key,
                            consumer_secret=consumer_secret,
                            access_token_method='GET',
                            access_token_params={'state': 'my_access_token'},
                            content_type="application/x-www-form-urlencoded"
                            )


# https://developers.google.com/accounts/docs/OAuth2WebServer
def Google(oauth, consumer_key, consumer_secret):
    return oauth.remote_app('Google',
                            base_url='https://accounts.google.com',
                            request_token_url=None,
                            access_token_url='/o/oauth2/token',
                            authorize_url='o/oauth2/auth',
                            consumer_key=consumer_key,
                            consumer_secret=consumer_secret,
                            access_token_method='POST',
                            request_token_params={'state': 'veryti_access_token'}
                            )


def bind_account(user_id, open_id, provider):
    s = g.db.session
    oauth_user = s.query(OAuthUserTable).filter_by(provider=provider, open_id=open_id).first()
    oauth_user.userId = user_id
    s.add(oauth_user)
    s.flush()


def unbind_account(user_id, open_id, provider):
    s = g.db.session
    oauth_user = g.db.session.query(OAuthUserTable).filter_by(userId=user_id, provider=provider, open_id=open_id).first()
    if oauth_user:
        oauth_user.userId = None
        s.add(oauth_user)
        s.flush()


def get_user_bindings(user_id):
    session = g.db.session
    rows = session.query(OAuthUserTable).filter_by(userId=user_id).all()
    result = {}
    for row in rows:
        values = result.get(row.provider)
        if values is None:
            values = [row]
            result[row.provider] = values
        else:
            values.append(row)
    return result


class OAuthBase(object):
    def __init__(self, oauth, provider):
        self.session = g.db.session
        self.oauth = oauth
        self.provider = provider

    def authorize_response(self, resp):
        open_id = self._handle_token(resp)
        if open_id is None:
            return None
        return self.get_oauth_user(open_id)

    def get_oauth_user(self, open_id):
        oauth_user = self.session.query(OAuthUserTable).filter_by(provider=self.provider, open_id=open_id).first()
        return oauth_user

    def _create_oauth_user(self, open_id, token, expires_in, refresh_token=None):
        oauth_user = OAuthUserTable()
        oauth_user.open_id = open_id
        oauth_user.provider = self.provider
        oauth_user.token = token
        oauth_user.expire_at = datetime.now() + timedelta(seconds=expires_in)
        oauth_user.refresh_token = refresh_token
        return oauth_user

    # To be override
    def _handle_token(self, resp):
        return None

    def get_provider_name(self):
        return None


class QQOAuthService(OAuthBase):
    def __init__(self, oauth):
        super(QQOAuthService, self).__init__(oauth, OAUTH_QQ_PROVIDER)

    def _handle_token(self, resp):
        access_token = resp.get('access_token')
        expires_in = resp.get('expires_in')
        session[QQ_OAUTH_SESSION_TOKEN_KEY] = (access_token, self.oauth.consumer_secret)
        open_id_response = self.oauth.request(url='https://graph.qq.com/oauth2.0/me', data={'access_token': access_token})
        open_id = None
        if open_id_response is not None and open_id_response.data is not None and len(open_id_response.data) > 0:
            data = open_id_response.raw_data
            start = data.index("{")
            end = data.rindex("}")
            data = data[start:end + 1]
            data = json.loads(data)
            open_id = data.get("openid")
        if open_id is None:
            return None

        oauth_user = self.get_oauth_user(open_id)
        user_info = self.__get_user_info(access_token, self.oauth.consumer_key, open_id)
        if oauth_user is None:
            oauth_user = self._create_oauth_user(open_id, access_token, int(expires_in))
            #user = UserManager().create_random_user(nick_name=self.__extract_nickname(user_info), avatar_url=self.__extract_avatar_url(user_info))
            #oauth_user.userId = user.userId
        self.__update_user(oauth_user, user_info)
        return open_id

    def __get_user_info(self, token, consumer_key, open_id):
        url = 'https://graph.qq.com/user/get_user_info'
        data = {'access_token': token, 'oauth_consumer_key': consumer_key, 'openid': open_id}
        user_info = self.oauth.request(url=url, data=data)
        if user_info.status == 200:
            info = json.loads(user_info.raw_data)
            return info
        return None

    def get_provider_name(self):
        return u"QQ"

    def __update_user(self, oauth_user, user_info):
        oauth_user.nick_name = self.__extract_nickname(user_info)
        gender = user_info.get('gender')
        if gender == u"男":
            oauth_user.gender = "M"
        elif gender == u"女":
            oauth_user.gender = "F"
        else:
            oauth_user.gender = "O"
        oauth_user.avatar_image_url = self.__extract_avatar_url(user_info)
        self.session.add(oauth_user)
        self.session.flush()

    @staticmethod
    def __extract_nickname(user_info):
        return user_info.get('nickname')

    @staticmethod
    def __extract_avatar_url(user_info):
        return user_info.get('figureurl_qq_1')


class GoogleOAuthService(OAuthBase):
    def __init__(self, oauth):
        super(GoogleOAuthService, self).__init__(oauth, OAUTH_GOOGLE_PROVIDER)

    def _handle_token(self, resp):
        access_token = resp.get('access_token')
        expires_in = resp.get('expires_in')
        refresh_token = resp.get('refresh_token')
        user_info = self.__get_user_info(access_token)
        open_id = user_info.get("id")
        if open_id is None:
            return None
        session[GOOGLE_OAUTH_SESSION_TOKEN_KEY] = (access_token, self.oauth.consumer_secret)
        oauth_user = self.get_oauth_user(open_id)
        if oauth_user is None:
            oauth_user = self._create_oauth_user(open_id, access_token, int(expires_in), refresh_token)
        self.__update_user(oauth_user, user_info)
        return open_id

    def __get_user_info(self, token):
        url = 'https://www.googleapis.com/oauth2/v1/userinfo'
        data = {'access_token': token}
        user_info_resp = self.oauth.request(url=url, data=data)
        if user_info_resp.status == 200:
            info = json.loads(user_info_resp.data)
            return info
        return None

    def __update_user(self, oauth_user, user_info):
        oauth_user.nick_name = user_info.get('name')
        oauth_user.email = user_info.get('email')
        oauth_user.email_verified = user_info.get('verified_email')
        oauth_user.avatar_image_url = user_info.get('picture')
        oauth_user.locale = user_info.get('locale')
        gender = user_info.get('gender')
        if gender == "male":
            oauth_user.gender = "M"
        elif gender == "female":
            oauth_user.gender = "F"
        else:
            oauth_user.gender = "O"
        self.session.add(oauth_user)
        self.session.flush()

    def get_provider_name(self):
        return u"Google"