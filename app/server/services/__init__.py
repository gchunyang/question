from flask import g


class BaseManager(object):
    def __init__(self):
        self.session = g.db.session
