# -*- coding: utf-8 -*-
import json
from datetime import datetime, timedelta

from werkzeug import urls

from server.services import BaseManager

from server.services.database.user_tables import UserTable, UserLoginHistoryTable
from server.errors import EmailNotRegisterError, PasswordWrongError, EmailFreqError, TokenExpireError, AlreadyVerifiedError, UserNotFoundError
from server.utils.flask_util import JSONSecureCookie
from server.utils.string_util import md5, create_uuid


class UserManager(BaseManager):
    _SESSION_NAME = "security_token"
    _USER_ID = "uid"
    _EMAIL = "email"
    _SECURE_PASSWORD = "sf#asfh*dss(s%sfsf"
    _SALT = "pwd_salt_$*1^23"

    def is_email_used(self, value):
        row = self.session.query(UserTable.userId).filter_by(email=value).first()
        return row is not None

    def is_name_used(self, value):
        row = self.session.query(UserTable.userId).filter_by(name=value).first()
        return row is not None

    def register(self, email, password, name=None):
        user = UserTable()
        user.userId = create_uuid()
        user.email = email.strip().lower()
        user.pwd = md5(self._SALT + password)
        user.name = self.create_temp_name(email) if name is None else name
        self.session.add(user)
        self.session.flush()
        return user

    def create_temp_name(self, email):
        name = email[:email.index('@')]
        i = 1
        while self.is_name_used(name):
            name += i
        return name

    def login(self, email, password, ip_address, errors):
        email = email.strip().lower()
        user = self.session.query(UserTable).filter_by(email=email).first()
        if user is None:
            errors.append(EmailNotRegisterError("name"))
            return errors
        else:
            if user.pwd == md5(self._SALT + password):
                if user.cont_fail_count > 0:
                    user.cont_fail_count = 0
                    self.session.add(user)
                history = UserLoginHistoryTable()
                history.userId = user.userId
                history.createdAt = datetime.now()
                history.ip = ip_address
                self.session.add(history)
                self.session.flush()
            else:
                errors.append(PasswordWrongError("pwd"))
                return errors
        return user

    def get_user_by_id(self, user_id):
        return self.session.query(UserTable).filter(UserTable.userId == user_id).first()

    def get_user_name(self, user_id):
        row = self.session.query(UserTable.name).filter(UserTable.userId == user_id).first()
        if row is not None:
            return row[0]
        return None

    def get_user_by_name(self, name):
        return self.session.query(UserTable).filter(UserTable.name == name).first()

    def get_user_id_by_name(self, name):
        row = self.session.query(UserTable.userId).filter(UserTable.name == name).first()
        if row is not None:
            return row[0]
        return None

    def get_users(self, user_id_list):
        rows = self.session.query(UserTable.userId, UserTable.name).filter(UserTable.userId.in_(user_id_list)).all()
        result = {}
        for row in rows:
            result[row[0]] = row[1]
        return result

    def change_password(self, user_id, pwd):
        user = self.session.query(UserTable).filter_by(userId=user_id).first()
        assert isinstance(pwd, str)
        user.pwd = md5(self._SALT + pwd)
        user.reset_pwd_token = None
        user.reset_pwd_send_at = None
        self.session.add(user)
        self.session.flush()

    def send_email_verification(self, user_id, url, content):
        user = self.session.query(UserTable).get(user_id)
        if user.email is None:
            return
        now = datetime.now()
        if user.last_verify_send_at is not None:
            delta = now - user.last_verify_send_at
            if (delta.total_seconds() / 60) < 10:
                return EmailFreqError()
        user.last_verify_send_at = now
        self.session.add(user)
        self.session.flush()
        return True

    def send_reset_password_email(self, config, user, subject, content):
        pass

    def get_reset_password_token(self, user):
        data_string = json.dumps({self._USER_ID: user.userId})
        sc = JSONSecureCookie({self._SESSION_NAME: data_string}, self._SECURE_PASSWORD)
        token = urls.url_quote(sc.serialize(datetime.utcnow() + timedelta(hours=2)))
        user.reset_pwd_token = token
        user.reset_pwd_send_at = datetime.now()
        self.session.add(user)
        self.session.flush()
        return user

    def validate_reset_password_token(self, token):
        data = self._read_secure_token(token)
        user_id = data.get(self._USER_ID)
        if not user_id:
            raise TokenExpireError()
        user = self.session.query(UserTable).get(user_id)
        if user.reset_pwd_token is None or urls.url_unquote(user.reset_pwd_token) != token:
            raise TokenExpireError()
        return user

    def get_verify_email_token(self, user):
        email = user.email
        data = {self._USER_ID: user.userId, self._EMAIL: email}
        data_string = json.dumps(data)
        now = datetime.utcnow()
        two_days = timedelta(days=2)
        sc = JSONSecureCookie({self._SESSION_NAME: data_string}, self._SECURE_PASSWORD)
        return urls.url_quote(sc.serialize(now + two_days))

    def _read_secure_token(self, secure_token):
        value = urls.url_unquote(secure_token)
        sc = JSONSecureCookie.unserialize(value, self._SECURE_PASSWORD)
        content = sc.get(self._SESSION_NAME)
        if content is not None:
            data = json.loads(content)
            return data
        else:
            raise TokenExpireError()

    def verify_email(self, secure_token):
        data = self._read_secure_token(secure_token)
        user_id = data.get(self._USER_ID)
        if not user_id:
            raise TokenExpireError()
        email = data.get(self._EMAIL)
        self.mark_email_verified(email, user_id)

    def mark_email_verified(self, email, user_id):
        email = email.lower()
        user = self.session.query(UserTable).filter_by(userId=user_id).filter_by(email=email).first()
        if user:
            if not user.email_verified:
                user.email_verified = True
                self.session.add(user)
                self.session.flush()
            else:
                raise AlreadyVerifiedError()
        else:
            raise UserNotFoundError()
