from datetime import datetime

from sqlalchemy.sql.functions import count

from server.errors import DataValueError
from server.helpers.login_helper import get_current_user_id
from server.services import BaseManager
from server.services.database.question_tables import QuestionTable, TagTable, QuestionTagTable, \
    CommentTable
from server.services.model.question_models import Question, Tag
from server.services.user_service import UserManager
from server.utils.string_util import create_uuid, extract_string, extract_list


class TagManager(BaseManager):
    def __init__(self):
        super(TagManager, self).__init__()

    def get_tag_by_name(self, name):
        return self.session.query(TagTable).filter_by(name=name).first()

    def get_tag_model(self, name):
        tag_model = self.get_tag_by_name(name)
        if tag_model is None:
            tag_model = TagTable()
            tag_model.name = name
            self.session.add(tag_model)
            self.session.flush()
        return tag_model

    def put_question_tag_relation(self, question_id, values):
        self.session.query(QuestionTagTable).filter(QuestionTagTable.qid == question_id).delete()
        for v in values:
            tag_id = self.get_tag_model(v).tagId
            model = QuestionTagTable()
            model.qid = question_id
            model.tagId = tag_id
            self.session.add(model)
        self.session.flush()

    def get_question_tag_ids(self, tags):
        rows = self.session.query(TagTable).filter(TagTable.name.in_(tags)).all()
        return [{"id": x.tagId, "name": x.name} for x in rows]

    def get_all_tag_and_count(self):
        query = self.session.query(TagTable.tagId, TagTable.name, count(1).label('total')) \
            .join(QuestionTagTable, QuestionTagTable.tagId == TagTable.tagId) \
            .join(QuestionTable, QuestionTable.qid == QuestionTagTable.qid) \
            .filter(QuestionTable.published == 1) \
            .group_by(TagTable.tagId)\
            .having(count(1) > 0)\
            .order_by('total desc')
        rows = query.all()
        return [Tag(r[0], r[1], r[2]) for r in rows]

    def get_user_tag_and_count(self, user_id):
        query = self.session.query(TagTable.tagId, TagTable.name, count(1).label('total')) \
            .join(QuestionTagTable, QuestionTagTable.tagId == TagTable.tagId) \
            .join(QuestionTable, QuestionTable.qid == QuestionTagTable.qid) \
            .filter(QuestionTable.author == user_id) \
            .group_by(TagTable.tagId)\
            .having(count(1) > 0)\
            .order_by('total desc')
        rows = query.all()
        return [Tag(r[0], r[1], r[2]) for r in rows]


class QuestionManager(BaseManager):
    def publish_question(self, question_id, value=True):
        question_model = self.session.query(QuestionTable).get(question_id)
        user_id = get_current_user_id()
        if question_model is None or question_model.author != user_id:
            return None
        question_model.published = value
        question_model.indexed = True
        question_model.reviewed = False

        self.session.add(question_model)
        self.session.flush()

    def save_question(self, data, errors):
        user_id = get_current_user_id()
        question_id = extract_string(data, "id", errors=errors)
        question_title = extract_string(data, "title", errors=errors, min_length=-1, max_length=400)
        question_answer = extract_string(data, "answer", errors=errors, min_length=-1, max_length=2000)
        question_tags = extract_list(data, "tags", errors=errors, min_length=0, nullable=True)
        if len(errors) > 0:
                return None

        now = datetime.now()
        if question_id is None:
            question_model = QuestionTable()
            question_model.qid = create_uuid()
            question_model.author = user_id
            question_model.title = question_title
            question_model.answer = question_answer
            question_model.set_tags(question_tags)
            question_model.createAt = now
            question_model.updateAt = now

            self.session.add(question_model)
            self.session.flush()
        else:
            question_model = self.session.query(QuestionTable).filter(QuestionTable.author == user_id, QuestionTable.qid == question_id).first()
            if question_model is None:
                return None
            question_model.updateAt = now
            if question_title is not None:
                question_model.title = question_title
            if question_answer is not None:
                question_model.answer = question_answer
            if question_tags is not None:
                question_model.set_tags(question_tags)
            self.session.add(question_model)
        self.session.flush()

        if question_tags is not None:
            tm = TagManager()
            tm.put_question_tag_relation(question_model.qid, question_tags)
            self.session.flush()

        return question_model.qid

    def get_question(self, question_id):
        current_user_id = get_current_user_id()
        q = self.session.query(QuestionTable).get(question_id)
        if q is None:
            return None
        if not q.published and (current_user_id is None or q.author != current_user_id):
            return None

        return self.__build_question_detail(q, current_user_id)

    def delete_question(self, question_id):
        current_user_id = get_current_user_id()
        self.session.query(QuestionTable).filter(QuestionTable.author == current_user_id).get(question_id).delete()

    def get_questions_for_anonymous(self, anonymous_query_filter):
        query = self.session.query(QuestionTable).filter(QuestionTable.indexed == 1, QuestionTable.published == 1)
        if anonymous_query_filter.of_user is not None:
            query = query.filter(QuestionTable.author == anonymous_query_filter.of_user)
        if anonymous_query_filter.tag is not None:
            tag = TagManager().get_tag_by_name(anonymous_query_filter.tag)
            if tag is not None:
                query = query.join(QuestionTagTable, QuestionTable.qid == QuestionTagTable.qid).filter(QuestionTagTable.tagId == tag.tagId)
            else:
                return []
        query = query.order_by(QuestionTable.createAt.desc())

        rows = query.slice(anonymous_query_filter.start, anonymous_query_filter.start + anonymous_query_filter.take).all()
        return self.__build_question_list(rows)

    def get_questions_for_member(self, member_query_filter):
        current_user_id = get_current_user_id()
        query = self.session.query(QuestionTable)
        if member_query_filter.tag is not None:
            tag = TagManager().get_tag_by_name(member_query_filter.tag)
            if tag is not None:
                query = query.join(QuestionTagTable, QuestionTable.qid == QuestionTagTable.qid).filter(QuestionTagTable.tagId == tag.tagId)
            else:
                return []

        query = query.filter(QuestionTable.author == current_user_id)
        if member_query_filter.is_draft:
            query = query.filter(QuestionTable.published == 0)
        else:
            query = query.filter(QuestionTable.published == 1)
        query = query.order_by(QuestionTable.createAt.desc())

        rows = query.slice(member_query_filter.start, member_query_filter.start + member_query_filter.take).all()
        return self.__build_question_list(rows)

    @staticmethod
    def __build_question_list(rows):
        author_ids = set([row.author for row in rows])
        authors = UserManager().get_users(author_ids)
        current_user_id = get_current_user_id()
        return [Question(row, authors.get(row.author), current_user_id) for row in rows]

    @staticmethod
    def __build_question_detail(question, current_user_id):
        user_name = UserManager().get_user_name(question.author)
        return Question(question, user_name, current_user_id)


class CommentManager(BaseManager):
    def add_comment(self, user_id, question_id, content, errors, thread_id=None):
        question_model = self.session.query(QuestionTable).get(question_id)
        if question_model is None:
            errors.append(DataValueError("questionId"))
            return errors

        comment_model = CommentTable()
        comment_model.commentId = create_uuid()
        comment_model.qid = question_id
        comment_model.threadId = thread_id
        comment_model.author = user_id
        comment_model.content = content
        comment_model.createAt = datetime.now()
        self.session.add(comment_model)
        self.session.flush()
        return comment_model

    def get_comment(self, question_id):
        return self.session.query(CommentTable).filter_by(questionId=question_id).all()

    def delete_comment(self, comment_id):
        self.session.query(CommentTable).filter_by(commentId=comment_id).delete()


class AnonymousQuestionQueryFilter(object):
    def __init__(self, of_user, tag, start, take):
        self.of_user = of_user
        self.tag = tag
        self.start = start
        self.take = take


class MemberQuestionQueryFilter(object):
    def __init__(self, is_draft, tag, start, take):
        self.is_draft = is_draft
        self.tag = tag
        self.start = start
        self.take = take


class PageFilter(object):
    # noinspection PyBroadException
    @staticmethod
    def parse(value):
        if value is not None:
            try:
                v = int(value)
                if v < 0:
                    return 0
                return v
            except:
                return 0
        return 0


class TagFilter(object):
    DEFAULT = None

    @staticmethod
    def parse(value):
        if value is not None:
            model = TagManager().get_tag_by_name(value)
            if model is None:
                return TagFilter.DEFAULT
            return model.tagId
        return TagFilter.DEFAULT


class BoolFilter(object):

    @staticmethod
    def parse(value):
        return bool(value)