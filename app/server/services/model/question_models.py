from server.utils.jinja_filter import to_markdown_excerpt, to_markdown


class Question(object):
    def __init__(self, question_table, author_name, current_user_id):
        self.id = question_table.qid
        self.author_id = question_table.author
        self.author = author_name
        self.title = question_table.title
        self.tags = question_table.get_tags()
        self.update_date = question_table.updateAt
        self.rating = question_table.rating
        self.fav_count = question_table.favCount
        self.excerpt = to_markdown_excerpt(question_table.answer)
        self.answer = to_markdown(question_table.answer)
        self.can_edit = question_table.author == current_user_id
        self.published = question_table.published
        self.__raw_answer = question_table.answer

    def to_editor_object(self):
        return {"id": self.id, "title": self.title, "answer": self.__raw_answer, "tags": self.tags, "published": self.published}


class Tag(object):
    def __init__(self, tid, name, count):
        self.id = tid
        self.name = name
        self.count = count


class Author(object):
    def __init__(self, author_id, name, desc):
        self.id = author_id
        self.name = name
        self.desc = desc