import os
from flask.ext.login import UserMixin

import httplib2
from sqlalchemy import Column, String, BOOLEAN, TEXT, DATETIME, SMALLINT, CHAR, Index, ForeignKey, DateTime, PrimaryKeyConstraint
from sqlalchemy.dialects.mysql import INTEGER

from server.app_globals import db
from server.services.database.custom_types import SimpleUUID, IPAddress


httplib2.CA_CERTS = os.path.join(
    os.path.dirname(os.path.abspath(__file__)), "cacert.pem")


class UserTable(db.Model, UserMixin):
    __tablename__ = 'user'
    __table_args__ = {'mysql_engine': 'InnoDB', 'mysql_charset': 'utf8'}
    userId = Column('id', SimpleUUID, primary_key=True, nullable=False, autoincrement=False)
    email = Column("email", String(120), unique=True, nullable=True, index=True)
    name = Column("name", String(200), unique=True, nullable=True, index=True)   # can be used for login
    pwd = Column(String(200), nullable=False)
    desc = Column(String(500))
    avatar_image_url = Column(String(300))
    gender = Column(CHAR(1))  # F: Female  M: Male
    email_verified = Column(BOOLEAN, default=False)
    mobile = Column(String(30))
    mobile_verified = Column(BOOLEAN, default=False)
    last_verify_send_at = Column(DATETIME)
    reset_pwd_token = Column(TEXT)
    reset_pwd_send_at = Column(DATETIME)
    remember_created_at = Column(DATETIME)
    cont_fail_count = Column(SMALLINT, default=0)
    active = Column(BOOLEAN, default=True)

    def get_id(self):
        return unicode(self.userId)

    def is_active(self):
        return self.active


class OAuthUserTable(db.Model):
    __tablename__ = 'user_oauth'
    __table_args__ = {'mysql_engine': 'InnoDB', 'mysql_charset': 'utf8'}
    oauth_id = Column('id', SimpleUUID, primary_key=True, nullable=False, autoincrement=False)
    provider = Column(CHAR(1), nullable=False)  # QQ:'Q'; Weibo:'W'; Baidu:'B'; Google:'G'; RenRen:'R'
    open_id = Column(String(200), nullable=False)
    userId = Column('user_id', SimpleUUID, ForeignKey('user.id', onupdate='CASCADE', ondelete='CASCADE'), index=True)
    token = Column(String(200), nullable=False)
    expire_at = Column(DATETIME)
    refresh_token = Column(String(200))
    gender = Column(CHAR(1))  # F: Female  M: Male  O: Other
    nick_name = Column(String(200))
    name = Column(String(200))
    email = Column(String(200))
    email_verified = Column(BOOLEAN, default=False)
    location = Column(String(200))
    locale = Column(String(20))
    timezone = Column(INTEGER)
    avatar_image_url = Column(String(300))

Index("idx_unique_openId_provider", OAuthUserTable.provider, OAuthUserTable.oauth_id, unique=True)


class UserLoginHistoryTable(db.Model):
    __tablename__ = 'user_login_history'
    __table_args__ = {'mysql_engine': 'InnoDB', 'mysql_charset': 'utf8'}
    userId = Column('user_id', SimpleUUID, ForeignKey('user.id', onupdate='CASCADE', ondelete='CASCADE'), index=True)
    createdAt = Column('created_at', DateTime, nullable=False)
    ip = Column('ip', IPAddress, nullable=False)
    __table_args__ = (PrimaryKeyConstraint(userId, createdAt),)