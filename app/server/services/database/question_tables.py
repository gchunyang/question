from sqlalchemy import Column, String, ForeignKey, DateTime, Text, Boolean, PrimaryKeyConstraint
from sqlalchemy.dialects.mysql import TINYINT, INTEGER, SMALLINT

from server.app_globals import db
from server.services.database.custom_types import SimpleUUID, IPAddress
from server.services.database.user_tables import UserTable


class QuestionState(object):
    DELETED = 0
    DRAFT = 1
    PUBLISH = 2
    INDEXED = 3
    RECOMMEND = 4

    @staticmethod
    def default():
        return QuestionState.PUBLISH


class QuestionTable(db.Model):
    __tablename__ = 'question'
    __table_args__ = {'mysql_engine': 'InnoDB', 'mysql_charset': 'utf8', 'extend_existing': True}
    qid = Column('id', SimpleUUID, primary_key=True, nullable=False, autoincrement=False)
    author = Column('author', SimpleUUID, ForeignKey(UserTable.userId, ondelete="CASCADE"), nullable=True)
    title = Column('title', String(1000), nullable=True)
    answer = Column('answer', Text, nullable=True)
    tags = Column('tags', Text, nullable=True)
    createAt = Column('create_at', DateTime, nullable=False)
    updateAt = Column('updated_at', DateTime, nullable=False)
    rating = Column('rating', TINYINT(unsigned=True), nullable=True)
    favCount = Column('fav_count', SMALLINT(unsigned=True), server_default="0", nullable=False)
    published = Column('published', Boolean, server_default="0", nullable=False)
    indexed = Column('indexed', Boolean, server_default="1", nullable=False)
    reviewed = Column('reviewed', Boolean, server_default="0", nullable=False)

    def set_tags(self, tag_list):
        if tag_list is not None:
            self.tags = ", ".join(tag_list) if len(tag_list) > 0 else ""

    def get_tags(self):
        if self.tags is not None and len(self.tags) > 0:
            return self.tags.split(", ")
        return []


class QuestionRateTable(db.Model):
    __tablename__ = 'question_rate'
    __table_args__ = {'mysql_engine': 'InnoDB', 'mysql_charset': 'utf8', 'extend_existing': True}
    qid = Column('id', SimpleUUID, ForeignKey(QuestionTable.qid, ondelete="CASCADE"), autoincrement=False, nullable=False)
    ip = Column('ip', IPAddress, nullable=False, autoincrement=False)
    usefulScore = Column('useful_score', TINYINT, nullable=True)
    clearScore = Column('clear_score', TINYINT, nullable=True)
    humorScore = Column('humor_score', TINYINT, nullable=True)
    updateAt = Column('updated_at', DateTime, nullable=False)
    __table_args__ = (PrimaryKeyConstraint(qid, ip),)


class QuestionFavTable(db.Model):
    __tablename__ = 'question_fav'
    __table_args__ = {'mysql_engine': 'InnoDB', 'mysql_charset': 'utf8', 'extend_existing': True}
    qid = Column('id', SimpleUUID, ForeignKey(QuestionTable.qid, ondelete="CASCADE"), nullable=False, autoincrement=False)
    uid = Column('uid', SimpleUUID, ForeignKey(UserTable.userId, ondelete="CASCADE"), nullable=True)
    createAt = Column('created_at', DateTime, nullable=False)
    __table_args__ = (PrimaryKeyConstraint(qid, uid),)


class TagTable(db.Model):
    __tablename__ = 'tag'
    __table_args__ = {'mysql_engine': 'InnoDB', 'mysql_charset': 'utf8', 'extend_existing': True}
    tagId = Column('id', INTEGER(unsigned=True), primary_key=True, nullable=False, autoincrement=True)
    name = Column('name', String(50), nullable=False, index=True, unique=True)


class QuestionTagTable(db.Model):
    __tablename__ = 'question_tag'
    __table_args__ = {'mysql_engine': 'InnoDB', 'mysql_charset': 'utf8', 'extend_existing': True}
    tagId = Column('id', INTEGER(unsigned=True), ForeignKey(TagTable.tagId, ondelete="CASCADE"), nullable=False)
    qid = Column('question_id', SimpleUUID, ForeignKey(QuestionTable.qid, ondelete="CASCADE"), nullable=False)
    __table_args__ = (PrimaryKeyConstraint(tagId, qid),)


class CommentTable(db.Model):
    __tablename__ = 'comment'
    __table_args__ = {'mysql_engine': 'InnoDB', 'mysql_charset': 'utf8', 'extend_existing': True}
    commentId = Column('id', SimpleUUID, primary_key=True, nullable=False, autoincrement=False)
    qid = Column('question_id', SimpleUUID, ForeignKey(QuestionTable.qid, ondelete="CASCADE"), nullable=False)
    threadId = Column('thread_id', SimpleUUID, ForeignKey(commentId, ondelete="CASCADE"), nullable=False)
    author = Column('author', SimpleUUID, ForeignKey(UserTable.userId, ondelete="CASCADE"), nullable=True)
    content = Column('answer', Text, nullable=False)
    createAt = Column('created_at', DateTime, nullable=False)
