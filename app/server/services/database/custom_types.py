import sqlalchemy.types as types
from sqlalchemy.dialects.mysql import BIGINT, INTEGER


class SimpleUUID(types.TypeDecorator):
    impl = BIGINT(unsigned=True)

    def process_bind_param(self, value, dialect):
        return long(value, 16)

    def process_result_value(self, value, dialect):
        return hex(value)[2:-1]


class IPAddress(types.TypeDecorator):
    impl = INTEGER(unsigned=True)

    def process_bind_param(self, value, dialect):
        import struct
        import socket
        return struct.unpack("!I", socket.inet_aton(value))[0]

    def process_result_value(self, value, dialect):
        import struct
        import socket
        return socket.inet_ntoa(struct.pack("!I", value))
