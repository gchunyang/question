from functools import wraps
from flask import Response
from flask.ext.login import current_user
import simplejson


def restful_api(func):
    @wraps(func)
    def decorated_view(*args, **kwargs):
        return create_response(func(*args, **kwargs))
    return decorated_view


def login_required_restful_api(func):
    @wraps(func)
    def decorated_view(*args, **kwargs):
        if current_user is not None and not current_user.is_authenticated():
            return create_json_response("", 401)
        return create_response(func(*args, **kwargs))

    return decorated_view


def create_json_response(data, status=200):
    return Response(data, status, mimetype='application/json')


def create_response(response):
    if response is None:
        return create_json_response("")
    else:
        return create_json_response(simplejson.dumps(response))