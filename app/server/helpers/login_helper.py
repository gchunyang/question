from flask.ext.login import current_user


def get_current_user_id():
    if current_user.is_authenticated():
        return current_user.userId
    return None


def get_current_user():
    if current_user.is_authenticated():
        return current_user
    return None
