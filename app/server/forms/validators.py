# -*- coding: utf-8 -*-
from validate_email import validate_email
from wtforms.validators import ValidationError

from server.services.user_service import UserManager


class NotUsed(object):
    def __init__(self, message=None):
        self.message = message

    def __call__(self, form, field):
        if self.is_used(field.data):
            message = self.message
            raise ValidationError(message)

    def is_used(self, data):
        return False


class EmailNotUsed(NotUsed):
    def is_used(self, data):
        return UserManager().is_email_used(data)


class NameNotUsed(NotUsed):
    def is_used(self, data):
        return UserManager().is_name_used(data)


class IsEmail(object):
    def __init__(self, message=None):
        self.message = message

    def __call__(self, form, field):
        if not validate_email(field.data):
            message = self.message
            raise ValidationError(message)
