# -*- coding: utf-8 -*-
from flask_wtf import Form
from wtforms import PasswordField
from wtforms.fields.html5 import EmailField
from wtforms.validators import DataRequired, Length
from server.forms.validators import IsEmail, EmailNotUsed

msg_email_required = u"邮箱不能为空"
msg_name_required = u"昵称不能为空"
msg_pwd_required = u"密码不能为空"
msg_length_6_30 = u"限定6～30个字符"
msg_name_used = u"该用户名已被使用，请再换一个"
msg_email_used = u"该邮箱已被使用，请再换一个"
msg_email_invalid = u"邮箱的格式不正确，或不存在"

label_email = u'邮箱'
label_pwd = u'密码'


class RegisterForm(Form):
    email = EmailField(label_email, validators=[DataRequired(message=msg_email_required), IsEmail(message=msg_email_invalid), EmailNotUsed(message=msg_email_used)])
    password = PasswordField(label_pwd, validators=[DataRequired(message=msg_pwd_required), Length(min=6, max=30, message=msg_length_6_30)])


class LoginForm(Form):
    email = EmailField(label_email, validators=[DataRequired(message=msg_email_required), IsEmail(message=msg_email_invalid)])
    password = PasswordField(label_pwd, validators=[DataRequired(message=msg_pwd_required), Length(min=6, max=30, message=msg_length_6_30)])


class ForgotPasswordForm(Form):
    email = EmailField(label_email, validators=[DataRequired(message=msg_email_required), IsEmail(message=msg_email_invalid)])



