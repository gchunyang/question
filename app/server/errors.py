# -*- coding: utf-8 -*-


class AppError(Exception):
    def __init__(self, field, msg):
        self.field = field
        self.message = msg
        super(AppError, self).__init__()


class DataRequireError(AppError):
    def __init__(self, field):
        super(DataRequireError, self).__init__(field, u"此项不能为空")


class DataRangeError(AppError):
    def __init__(self, field, min, max):
        msg = None
        if min is not None and max is not None:
            msg = u"范围在{0}~{1}之间".format(min, max)
        elif min is not None:
            msg = u"不能小于{0}".format(min)
        elif max is not None:
            msg = u"不能大于{0}".format(max)
        super(DataRangeError, self).__init__(field, msg)


class StringLengthError(AppError):
    def __init__(self, field, min, max):
        super(StringLengthError, self).__init__(field, u"{0}~{1}个字符".format(min, max))


class ListMinLengthError(AppError):
    def __init__(self, field, min):
        super(ListMinLengthError, self).__init__(field, u"至少要有{0}个元素".format(min))


class DataTypeError(AppError):
    def __init__(self, field, type):
        super(DataTypeError, self).__init__(field, u"必须为{0}类型".format(type.__name__))


class DataValueError(AppError):
    def __init__(self, field):
        super(DataValueError, self).__init__(field, u"数值不正确")


class EmailNotRegisterError(AppError):
    def __init__(self, field):
        super(EmailNotRegisterError, self).__init__(field, u"该邮箱尚未注册，请检查是否拼写错误或重新注册")


class EmailUsedError(AppError):
    def __init__(self, field, email):
        super(EmailUsedError, self).__init__(field, u"邮箱{0}已经被注册，若您忘记密码，请使用密码寻回功能".format(email))


class UserNameUsedError(AppError):
    def __init__(self, field, name):
        super(UserNameUsedError, self).__init__(field, u"用户名{0}已经被使用，请另换一个".format(name))


class PasswordWrongError(AppError):
    def __init__(self, field):
        super(PasswordWrongError, self).__init__(field, u"密码错误")


class NotSignInError(AppError):
    def __init__(self):
        super(NotSignInError, self).__init__(None, u"尚未登录")


class SpamError(AppError):
    def __init__(self):
        super(SpamError, self).__init__(None, u"垃圾信息提交")


class CrsfError(AppError):
    def __init__(self):
        super(CrsfError, self).__init__(None, u"CRSF攻击")


class DuplicatedSubmitError(AppError):
    def __init__(self):
        super(DuplicatedSubmitError, self).__init__(None, u"数据重复提交")


class PrivilegeError(AppError):
    def __init__(self):
        super(PrivilegeError, self).__init__(None, u"没有权限")


class UserNotFoundError(AppError):
    def __init__(self):
        super(UserNotFoundError, self).__init__(None, u"用户不存在")


class AlreadyVerifiedError(AppError):
    def __init__(self):
        super(AlreadyVerifiedError, self).__init__(None, u"您的账户已经验证过了")


class EmailFreqError(AppError):
    def __init__(self):
        super(EmailFreqError, self).__init__(None, u"验证邮件发送太频繁，请稍后再试")


class TokenExpireError(AppError):
    def __init__(self):
        super(TokenExpireError, self).__init__(None, u"Token过期")
