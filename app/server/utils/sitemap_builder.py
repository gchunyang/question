from server.utils import date_util


class SitemapBuilder(object):
    FREQ_ALWAYS = "always"
    FREQ_HOURLY = "hourly"
    FREQ_DAILY = "daily"
    FREQ_WEEKLY = "weekly"
    FREQ_MONTHLY = "monthly"
    FREQ_YEARLY = "yearly"
    FREQ_NEVER = "never"

    def __init__(self):
        self.root = []
        self.root.append('<?xml version="1.0" encoding="UTF-8"?><urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">')

    def append(self, url, modified_at, priority, freq):
        self.root.append("<url>")
        self.root.append("<loc>")
        self.root.append(url)
        self.root.append("</loc>")
        self.root.append("<lastmod>")
        self.root.append(date_util.format_date(modified_at))
        self.root.append("</lastmod>")
        self.root.append("<changefreq>")
        self.root.append(freq)
        self.root.append("</changefreq>")
        self.root.append("<priority>")
        self.root.append(priority)
        self.root.append("</priority>")
        self.root.append("</url>")

    def to_xml_string(self):
        self.root.append("</urlset>")
        return "".join(self.root)
