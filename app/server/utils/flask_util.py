import json
from flask import request
from werkzeug.contrib.securecookie import SecureCookie


def tear_down_request(exception, g):
    if not exception:
        if hasattr(g, 'db') and g.db.session.is_active:
            g.db.session.commit()
    else:
        if hasattr(g, 'db'):
            g.db.session.rollback()


def fault(error):
    result = json.dumps({"success": False, "message": error.message})
    return result


def success(message, data=None):
    result = json.dumps({"success": True, "message": message, "data": data})
    return result


def errors_to_dict(errors):
    result = {}
    if errors is None or not isinstance(errors, list):
        return result
    for e in errors:
        result[e.field] = e
    return result


def get_int_arg(name, default):
    value = request.args.get(name)
    if value is None or len(value) < 1:
        return default
    try:
        result = int(value)
        if result < 0:
            return default
        return result
    except ValueError:
        return default


class Pagination(object):
    pageIndex = 1
    totalPageNum = 1

    visible = False
    hasPrevious = False
    hasNext = False
    frontSection = None
    middleSection = None
    lastSection = None

    urlPattern = "{0}"

    def getUrl(self, page):
        return self.urlPattern.format(page)

    def build(self, pageIndex, totalPageNum):
        self.pageIndex = pageIndex
        self.totalPageNum = totalPageNum
        self.visible = (totalPageNum > 1)
        self.hasPrevious = (pageIndex > 1)
        self.hasNext = (pageIndex < totalPageNum)

        if totalPageNum < 6:
            self.frontSection = range(1, totalPageNum+1)
        else:
            previous = (pageIndex - 3)
            end = (pageIndex + 3)
            if previous <= 1 and end >= totalPageNum:
                self.frontSection = range(1, totalPageNum+1)
            elif previous <= 1:
                self.frontSection = range(1, pageIndex+3)
                self.middleSection = None
                self.lastSection = [totalPageNum]
            elif end >= totalPageNum:
                self.frontSection = [1]
                self.middleSection = None
                self.lastSection = range(pageIndex-2, totalPageNum+1)
            else:
                self.frontSection = [1]
                self.middleSection = range(pageIndex-2, pageIndex+3)
                self.lastSection = [totalPageNum]


class JSONSecureCookie(SecureCookie):
    serialization_method = json