# -*- coding: utf-8 -*-

def format_date_time(d):
    if not d:
        return None
    return d.strftime('%Y-%m-%d %H:%M:%S')


def format_date(d):
    if not d:
        return None
    return d.strftime('%Y-%m-%d')

