from email.mime.text import MIMEText
import logging
import smtplib


class UnicodeMailLogger(logging.Handler):
    def __init__(self, mailhost, fromaddr, toaddrs, subject,
                 credentials=None, secure=None):
        logging.Handler.__init__(self)
        if isinstance(mailhost, tuple):
            self.mailhost, self.mailport = mailhost
        else:
            self.mailhost, self.mailport = mailhost, None
        if isinstance(credentials, tuple):
            self.username, self.password = credentials
        else:
            self.username = None
        self.fromaddr = fromaddr
        if isinstance(toaddrs, basestring):
            toaddrs = [toaddrs]
        self.toaddrs = toaddrs
        self.subject = subject
        self.secure = secure

    def emit(self, record):
        try:
            msg = self.format(record)
            sendEmail(self.fromaddr, self.mailhost, self.username, self.password, self.toaddrs, self.subject, msg, "plain")
        except (KeyboardInterrupt, SystemExit):
            raise
        except Exception:
            self.handleError(record)


def sendHtmlEmail(config, toList, subject, htmlContent):
    sendEmail(config.EMAIL_ME, config.EMAIL_HOST, config.EAMIL_USER, config.EMAIL_PWD, toList, subject, htmlContent, 'html')


def sendPlainEmail(config, toList, subject, content):
    sendEmail(config.EMAIL_ME, config.EMAIL_HOST, config.EAMIL_USER, config.EMAIL_PWD, toList, subject, content, "plain")


def sendEmail(from_addr, host_addr, user_name, password, toList, subject, htmlContent, contentType="html"):
    msg = MIMEText(htmlContent, contentType, 'utf-8')
    msg['Subject'] = subject
    msg['From'] = from_addr
    msg['To'] = ";".join(toList)
    s = smtplib.SMTP()
    s.connect(host_addr)
    if user_name is not None:
        s.login(user_name, password)
    s.sendmail(from_addr, toList, msg.as_string())
    s.quit()


def getEmailSignInUrl(email):
    index = email.index("@")
    suffix = email[index+1:]
    if suffix == "163.com":
        return "http://mail.163.com/"
    elif suffix == "126.com":
        return "http://mail.126.com/"
    elif suffix == "188.com":
        return "http://mail.188.com/"
    elif suffix == "qq.com":
        return "https://mail.qq.com"
    elif suffix == "139.com":
        return "http://mail.10086.cn/"
    elif suffix == "gmail.com":
        return "https://mail.google.com"
    elif suffix in ["sina.cn", "sina.com"]:
        return "http://mail.sina.com.cn/"
    elif suffix in ["yahoo.com.cn", "yahoo.cn"]:
        return "http://mail.cn.yahoo.com/"
    elif suffix in ["hotmail.com", "live.cn", "live.com"]:
        return "http://mail.hotmail.com"
    elif suffix == "sohu.com":
        return "http://mail.sohu.com/"
    elif suffix == "21cn.com":
        return "http://mail.21cn.com/"
    elif suffix in ["tom.cn", "vip.tom.com", "tom.com"]:
        return "http://mail.tom.com/"
    elif suffix in ["263.net", "263.net.cn"]:
        return "http://mail.263.com/"
    return None