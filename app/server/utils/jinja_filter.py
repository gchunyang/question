# -*- coding: utf-8 -*-
from datetime import datetime

import markdown2

from server.errors import AppError
from server.utils.string_util import is_none_or_empty


def handle_error_code(value):
    if value is None:
        return ""
    if isinstance(value, AppError):
        return value.message
    return ""


def none_to_empty(value):
    return "" if is_none_or_empty(value) else value


EXCERPT_SEPARATOR = "<!--more-->"


def to_markdown(value):
    if value is None or len(value) < 1:
        return ""
    value = value.replace(EXCERPT_SEPARATOR, "")
    return markdown2.markdown(value, extras=["fenced-code-blocks", "code-friendly"])


def to_markdown_excerpt(value):
    if value is None or len(value) < 1:
        return ""
    index = value.find(EXCERPT_SEPARATOR)
    result = value[:index] if index > 0 else value
    return markdown2.markdown(result, extras=["fenced-code-blocks", "code-friendly"])


def friendly_date(value):
    if value is None:
        return ""
    now = datetime.now()
    timedelta = now - value
    days = timedelta.days
    if days < 31:
        if days < 1:
            return u"今天"
        elif days is 1:
            return u"昨天"
        elif days < 7:
            return u"{0}天前".format(days)
        else:
            return u"{0}周前".format(days / 7)
    else:
        if days < 365:
            return u"{0}个月前".format(days / 30)
        else:
            return u"{0}年前".format(days / 365)
