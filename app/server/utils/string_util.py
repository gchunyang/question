# -*- coding: utf-8 -*-
import hashlib
import re
import time
import random

from server.errors import DataRequireError, DataTypeError, ListMinLengthError, DataRangeError, StringLengthError


def is_string(value):
    return isinstance(value, unicode) or isinstance(value, str)


def is_none_or_empty(value):
    if not value:
        return True
    if is_string(value) and len(value) < 1:
            return True
    return False


def is_not_none_or_empty(value):
    return not is_none_or_empty(value)


def create_uuid():
    ts = int(time.time() * 1000) - 1400000000000
    value = 0
    for i in range(0, 10):
        shift = i * 3
        right = (ts & (7 << shift)) >> shift
        left = random.randint(0, 1) << 3
        value |= (left | right) << (i * 4)
    for i in range(0, 6):
        shift = 10 * 3 + i * 2
        right = (ts & (3 << shift)) >> shift
        left = random.randint(0, 3) << 2
        value |= (left | right) << ((10 + i) * 4)
    return hex(value)[2:-1]


def extract_string(data, key, errors, min_length=-1, max_length=100, trim=True):
    value = data.get(key)
    if value is None:
        if min_length >= 0:
            errors.append(DataRequireError(key))
            return None
    else:
        if trim:
            value = value.strip()
        if min_length > 0:
            length = len(value)
            if length < 1:
                errors.append(DataRequireError(key))
                return value
            elif max_length < length or length < min_length:
                errors.append(StringLengthError(key, min_length, max_length))
                return value
    return value


def extract_int(data, key, errors, nullable=False, min_value=None, max_value=None):
    value = data.get(key)
    if is_string(value) and is_none_or_empty(value):
        value = None
    if value is None:
        if not nullable:
            errors.append(DataRequireError(key))
        return None
    else:
        if not isinstance(value, int):
            value = int(value)
        if (min_value and value < min_value) or (max_value and value > max_value):
            errors.append(DataRangeError(key, min_value, max_value))
            return value
    return value


def extract_bool(data, key, errors, nullable=False, default=False):
    value = data.get(key)
    if value is None:
        if not nullable:
            errors.append(DataRequireError(key))
            return None
        else:
            return default
    else:
        if is_string(value):
            value = value.lower()
            if value == "on" or value == "true":
                value = True
            else:
                value = False
        elif not isinstance(value, bool):
            value = False
    return value


def extract_list(data, key, errors, nullable=False, min_length=0, trim=True):
    values = data.get(key)
    if values is None:
        if not nullable:
            errors.append(DataRequireError(key))
        return None
    else:
        if not isinstance(values, list):
            errors.append(DataTypeError(key, list))
            return values
        if len(values) < min_length:
            errors.append(ListMinLengthError(key, min_length))
            return values
        result = []
        for item in values:
            if is_string(item):
                if trim:
                    item = item.strip()
                if len(item) > 0:
                    result.append(item)
            else:
                result.append(item)
        return result

email_re = re.compile(
    r"(^[-!#$%&'*+/=?^_`{}|~0-9A-Z]+(\.[-!#$%&'*+/=?^_`{}|~0-9A-Z]+)*"  # dot-atom
    r'|^"([\001-\010\013\014\016-\037!#-\[\]-\177]|\\[\001-011\013\014\016-\177])*"'  # quoted-string
    r')@(?:[A-Z0-9](?:[A-Z0-9-]{0,61}[A-Z0-9])?\.)+[A-Z]{2}\.?$', re.IGNORECASE)  # domain


def is_email_address(email):
    if is_none_or_empty(email):
        return False
    if len(email) > 7:
        if email_re.match(email) is not None:
            return True
    return False


def md5(value):
    m = hashlib.md5()
    m.update(value.encode('utf-8'))
    return m.hexdigest()


_separators = [u"，", u"；", u"。", u"：", ";", ":"]


def parseTags(tagStr):
    if is_none_or_empty(tagStr):
        return []
    for sep in _separators:
        tagStr = tagStr.replace(sep, ",")
    return filter(is_not_none_or_empty, [t.strip() for t in tagStr.split(',')])