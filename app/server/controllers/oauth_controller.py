# -*- coding: utf-8 -*-

from flask import Blueprint, session, request, redirect, render_template
from flask_oauthlib.client import OAuth


oauth_pages = Blueprint("oauth_pages", __name__, template_folder='templates')

oauth = OAuth()
qq_client = QQ(oauth, "100313402", "c31125c9bf33cadb4d148683d86e5550")
SESSION_OAUTH_BIND_KEY = "oauth_bind"


@qq_client.tokengetter
def get_qq_oauth_token(token=None):
    return session.get(QQ_OAUTH_SESSION_TOKEN_KEY)


@oauth_pages.route("/qq_oauth")
def qq_oauth_page():
    redirect_uri = "http://veryti.com/qq-authorized"
    return qq_client.authorize(callback=redirect_uri)


@oauth_pages.route("/qq-authorized")
@qq_client.authorized_handler
def qq_authorized_handler(resp):
    service = QQOAuthService(qq_client)
    oauth_user = service.authorize_response(resp)
    if oauth_user is not None:
        user = UserManager().get_user_by_id(oauth_user.userId)
        create_sign_session(user)
        return redirect("/")
    return redirect("/")
