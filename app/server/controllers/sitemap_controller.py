from flask import Blueprint, Response
from server.utils.sitemap_builder import SitemapBuilder
from server.services.question_services import QuestionManager
from flask import request

sitemap_pages = Blueprint("trivial_pages", __name__, template_folder='templates')


@sitemap_pages.route('/sitemap.xml')
def get_xml_sitemap():
    host_url = request.host_url
    builder = SitemapBuilder()
    qm = QuestionManager()
    rows = qm.get_all_question_id_list()
    for row in rows:
        builder.append("{0}question/{1}".format(host_url, row[0]), row[1], "1.0", SitemapBuilder.FREQ_WEEKLY)
    return Response(builder.to_xml_string(), mimetype='text/xml')