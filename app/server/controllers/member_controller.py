from flask import render_template
from flask.blueprints import Blueprint
from flask.ext.login import login_required

from server.helpers.login_helper import get_current_user_id
from server.services.question_services import QuestionManager, MemberQuestionQueryFilter, TagManager


member_pages = Blueprint("member_pages", __name__, template_folder='templates', url_prefix="/member")


@member_pages.context_processor
def inject_tags():
    tags = TagManager().get_user_tag_and_count(get_current_user_id())
    return dict(tags=tags, current_user_id=get_current_user_id())


@member_pages.route('/create_question')
@member_pages.route('/edit_question')
@login_required
def get_question_editor():
    return render_template("member/editor.html")


@member_pages.route('/questions')
@login_required
def get_member_draft_questions():
    return get_member_questions()


@member_pages.route('/questions/tag/<tag>')
@login_required
def get_member_questions_with_tag(tag):
    return get_member_questions(tag)


def get_member_questions(tag=None):
    qm = QuestionManager()
    query = MemberQuestionQueryFilter(is_draft=True, tag=tag, start=0, take=20)
    draft_questions = qm.get_questions_for_member(query)
    return render_template("member/index.html", drafts=draft_questions)



