from flask import render_template
from flask.blueprints import Blueprint
from flask.ext.login import login_required

from server.services.question_services import QuestionManager, MemberQuestionQueryFilter
from server.utils.flask_util import get_int_arg


member_APIs = Blueprint("member_APIs", __name__, template_folder='templates', url_prefix="/api/member")


@member_APIs.route('/questions/draft')
@member_APIs.route('/questions/draft/tag/<tag>')
@login_required
def get_draft_question(tag=None):
    return get_member_questions(True, tag)


@member_APIs.route('/questions/published')
@member_APIs.route('/questions/published/tag/<tag>')
@login_required
def get_published_question(tag=None):
    return get_member_questions(False, tag)


def get_member_questions(is_draft, tag):
    start = get_int_arg("start", 0)
    take = get_int_arg("take", 20)
    qm = QuestionManager()
    query = MemberQuestionQueryFilter(is_draft=is_draft, tag=tag, start=start, take=take)
    draft_questions = qm.get_questions_for_member(query)
    return render_template("member/entries.html", questions=draft_questions)


