from flask import render_template, redirect
from flask.blueprints import Blueprint
from server.helpers.login_helper import get_current_user_id
from server.services.model.question_models import Author

from server.services.question_services import QuestionManager, AnonymousQuestionQueryFilter, TagManager
from server.services.user_service import UserManager
from server.utils.flask_util import get_int_arg


main_pages = Blueprint("main_pages", __name__, template_folder='templates')


@main_pages.context_processor
def inject_tags():
    tags = TagManager().get_all_tag_and_count()
    return dict(tags=tags, current_user_id=get_current_user_id())


@main_pages.route('/')
@main_pages.route('/questions')
def index():
    return get_questions(None)


@main_pages.route('/questions/author/<user_id>')
def questions_by_user(user_id):
    user = UserManager().get_user_by_id(user_id)
    author = Author(user_id, user.name, user.desc)
    return get_questions(of_user=user_id, author=author)


@main_pages.route('/questions/tag/<tag>')
def questions_by_tag(tag):
    return get_questions(tag=tag)


# How to use the site
@main_pages.route('/help')
def questions_by_help():
    user_id = UserManager().get_user_id_by_name("Help")
    return get_questions(user_id)


# About the site: history, ideas, philosophy
@main_pages.route('/about')
def questions_by_about():
    user_id = UserManager().get_user_id_by_name("About")

    return get_questions(user_id)


@main_pages.route('/question/<qid>')
def open_question(qid):
    qm = QuestionManager()
    q = qm.get_question(qid)
    if q is None:
        return redirect("/404.html")
    user = UserManager().get_user_by_id(q.author_id)
    author = Author(user.userId, user.name, user.desc)
    return render_template("main/question.html", question=q, author=author)


def get_questions(of_user=None, tag=None, author=None):
    start = get_int_arg("start", 0)
    take = get_int_arg("take", 20)
    qm = QuestionManager()
    query = AnonymousQuestionQueryFilter(of_user=of_user, tag=tag, start=start, take=take)
    questions = qm.get_questions_for_anonymous(query)
    return render_template("main/index.html", questions=questions, tag=tag, author=author)
