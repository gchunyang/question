from flask import request
from flask.blueprints import Blueprint

from server.helpers.api_helper import login_required_restful_api
from server.services.question_services import QuestionManager


main_APIs = Blueprint("main_APIs", __name__, template_folder='templates', url_prefix="/api")


@main_APIs.route('/question/<qid>')
@login_required_restful_api
def get_question_for_edit(qid):
    qm = QuestionManager()
    q = qm.get_question(qid)
    return q.to_editor_object()


@main_APIs.route('/question', methods=["POST", "PUT", "PATCH"])
@login_required_restful_api
def create_question():
    errors = []
    qm = QuestionManager()
    qid = qm.save_question(request.get_json(), errors)
    return {"id": qid}


@main_APIs.route('/question/<qid>', methods=["DELETE"])
@login_required_restful_api
def delete_question(qid):
    qm = QuestionManager()
    qm.delete_question(qid)
    return ""


@main_APIs.route('/publish/question/<qid>')
@login_required_restful_api
def publish_question(qid):
    qm = QuestionManager()
    qm.publish_question(qid)
    return ""


@main_APIs.route('/un-publish/question/<qid>')
@login_required_restful_api
def un_publish_question(qid):
    qm = QuestionManager()
    qm.publish_question(qid, False)
    return ""

