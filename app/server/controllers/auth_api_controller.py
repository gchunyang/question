# -*- coding: utf-8 -*-
from flask import Blueprint
from flask.ext.login import current_user

from server.helpers.api_helper import restful_api


auth_apis = Blueprint("auth_api", __name__, url_prefix="/api", template_folder='templates')
