# -*- coding: utf-8 -*-
from flask import Blueprint, render_template, request, redirect, flash, url_for, session, current_app
from flask.ext.login import login_user, logout_user
from server.forms.auth_forms import LoginForm, RegisterForm
from flask.ext.login import LoginManager
from server.services.user_service import UserManager


auth_pages = Blueprint("auth_pages", __name__, template_folder='templates')

login_manager = LoginManager()
login_manager.login_view = "auth_pages.login"
login_manager.session_protection = "strong"


@login_manager.user_loader
def load_user(user_id):
    return UserManager().get_user_by_id(user_id)


@auth_pages.route("/signin", methods=["GET", "POST"])
def login():
    form = LoginForm()
    if form.validate_on_submit():
        um = UserManager()
        errors = []
        user = um.login(email=form.email.data, password=form.password.data, ip_address=request.remote_addr, errors=errors)
        if len(errors) > 0:
            flash(u"用户名或密码错误")
        else:
            login_user(user)
            flash("Logged in successfully.")
            redirect_to_index = redirect(request.args.get("next") or url_for("main_pages.index"))
            response = current_app.make_response(redirect_to_index)
            response.set_cookie('logged_in', value='1')
            return response
    return render_template("auth/signin.html", form=form)


@auth_pages.route('/signup', methods=["GET", "POST"])
def register():
    form = RegisterForm()
    if form.validate_on_submit():
        um = UserManager()
        um.register(email=form.email.data, password=form.password.data)
        return redirect(request.args.get("next") or url_for("home_pages.index"))
    return render_template("auth/signup.html", form=form)


@auth_pages.route('/logout')
def logout():
    logout_user()
    response = current_app.make_response(redirect("/"))
    response.set_cookie('logged_in', value='0')
    return response