import os

from flask import Flask
from flask import g
from werkzeug.wsgi import SharedDataMiddleware

from config import QuestionConfig
from server import app_globals
from server.controllers.admin_controller import admin_pages
from server.controllers.auth_api_controller import auth_apis
from server.controllers.main_api_controller import main_APIs
from server.controllers.main_controller import main_pages
from server.controllers.auth_controller import auth_pages, login_manager
from server.controllers.member_api_controller import member_APIs
from server.controllers.member_controller import member_pages
from server.controllers.sitemap_controller import sitemap_pages
from server.utils.jinja_filter import friendly_date, none_to_empty, handle_error_code, to_markdown, to_markdown_excerpt
from server.utils import flask_util

app = Flask(__name__, static_folder='public')
QuestionConfig().init_app(app)
login_manager.init_app(app)

app.register_blueprint(auth_pages)
app.register_blueprint(main_pages)
app.register_blueprint(main_APIs)
app.register_blueprint(member_pages)
app.register_blueprint(member_APIs)
app.register_blueprint(sitemap_pages)
app.register_blueprint(admin_pages)
app.register_blueprint(auth_apis)

app.jinja_env.filters['friendly_date'] = friendly_date
app.jinja_env.filters['none_to_empty'] = none_to_empty
app.jinja_env.filters['handle_error_code'] = handle_error_code
app.jinja_env.filters['markdown'] = to_markdown
app.jinja_env.filters['excerpt'] = to_markdown_excerpt

@app.before_request
def before_request():
    g.db = app_globals.db


@app.teardown_request
def teardown_request(exception):
    flask_util.tear_down_request(exception, g)


if __name__ == '__main__':
    app.debug = True

    app.wsgi_app = SharedDataMiddleware(app.wsgi_app, {
        '/': os.path.join(os.path.dirname(__file__), 'public')
    })
    app.run(port=5000)